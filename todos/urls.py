from django.urls import path
from todos.views import todo_list, todo_list_detail, create_todos, update_TodoList, delete_list

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todos, name="todo_list_create"),
    path("<int:id>/edit/", update_TodoList, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
]
