from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.
def todo_list(request):
  todos = TodoList.objects.all()
  context = {
    "todo_list": todos,
  }
  return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
  todo_list=get_object_or_404(TodoList, id=id)
  context={
    "todo_list_detail": todo_list,
  }
  return render(request, "todos/detail.html", context)

def create_todos(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todos = form.save(False)
            todos.author = request.user
            todos.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def update_TodoList(request, id):
  todo_list = get_object_or_404(TodoList, id=id)
  if request.method == "POST":
    form = TodoListForm(request.POST, instance=todo_list)
    if form.is_valid():
      todo_list = form.save()
      return redirect("todo_list_detail", id=id)
  else:
    form = TodoListForm(instance=todo_list)
  context = {
    "form": form,
    "todo_list": todo_list,
  }
  return render(request, "todos/edit.html", context)

def delete_list(request, id):
   todo_list = TodoList.objects.get(id=id)
   if request.method == "POST":
      todo_list.delete()
      return redirect("todo_list_list")
   return render(request, "todos/delete.html")
